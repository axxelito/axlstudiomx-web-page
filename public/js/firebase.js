// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: "AIzaSyDB1marJ7VPmp12_gEcarVGNKwPZkXKBNY",
    authDomain: "axlstudiomx-web-page.firebaseapp.com",
    projectId: "axlstudiomx-web-page"
});

var db = firebase.firestore();

function send() {
    var mName = document.getElementById('name').value;
    var mEmail = document.getElementById('email').value;
    var mPhone = document.getElementById('phone').value;
    var mMessage = document.getElementById('message').value;
    var mWarning = document.getElementById('warning');

    db.collection("messages").add({
        name: mName,
        email: mEmail,
        phone: mPhone,
        message: mMessage
    }).then(function (docRef) {
        console.log("Your message was send");
        var mName = document.getElementById('name').value = '';
        var mEmail = document.getElementById('email').value = '';
        var mPhone = document.getElementById('phone').value = '';
        var mMessage = document.getElementById('message').value = '';
        mWarning.classList.remove('warning-msg');
    }).catch(function (error) {
        console.error("Error adding document: ", error);
    });
}
